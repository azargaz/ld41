﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public int lives = 3;
    public int scoreValue = 100;

    EnemyShooter shooter;
    SpriteRenderer sprite;

    [HideInInspector]
    public bool isOnStage = false;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        shooter = GetComponent<EnemyShooter>();
    }

    void Update()
    {
        isOnStage = shooter.isOnStage;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 11)
        {
            Damage();
        }
    }

    IEnumerator DamageEffect()
    {
        sprite.color = new Color(0.7f, 0.7f, 0.7f);
        yield return new WaitForSeconds(0.25f);
        sprite.color = new Color(1f, 1f, 15f);
    }

    void Damage()
    {        
        if (lives > 0)
            lives--;
        
        if(lives <= 0)
            Desutroy();

        StartCoroutine(DamageEffect());
    }

    public void Disappear()
    {
        Destroy(gameObject);
    }

    void Desutroy()
    {
        PlayerStats.ps.AddScore(scoreValue);
        Destroy(gameObject);
    }
}
