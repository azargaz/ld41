﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

    public static WaveManager wm;

    int numberOfWaves;
    public static int waveNumber = 0;
    public int timePerWave = 30;
    public float delayBetweenWaves = 3f;
    float delayLeft = 0f;
    float timeLeft = 0f;

    public EnemyWave[] waves;
    List<Transform> spawnPoints = new List<Transform>();

    List<EnemyStats> spawnedEnemies = new List<EnemyStats>();
    HashSet<int> usedSpawnPoints = new HashSet<int>();

    [System.Serializable]
    public class EnemyWave
    {
        public EnemyStats[] enemies;
    }

	void Start ()
    {
        wm = this;

        waveNumber = 0;

        foreach (Transform child in transform)
        {
            spawnPoints.Add(child);
        }

        numberOfWaves = waves.Length - 1;
        StartCoroutine(NextWave());
    }

    bool lastEnemiesReady = false;
    bool enemiesReady = false;

	void Update ()
    {
        if(!anyEnemyIsReady())
            TurnManager.currentTurn = TurnManager.Turn.disabled;

        enemiesReady = anyEnemyIsReady();

        if(lastEnemiesReady != enemiesReady && TurnManager.currentTurn == TurnManager.Turn.disabled)
            TurnManager.tm.EndTurn(TurnManager.Turn.player);

        lastEnemiesReady = enemiesReady;

        if (timeLeft > 0)
        {
            if (TurnManager.currentTurn == TurnManager.Turn.enemy)
                timeLeft -= Time.deltaTime;
        }
        else
        {
            TurnManager.currentTurn = TurnManager.Turn.disabled;

            if (delayLeft > 0)
                delayLeft -= Time.deltaTime;
            else
                StartCoroutine(NextWave());
        }

        if (EnemiesAlive() <= 0 && waveNumber > 0)
        {
            timeLeft = 0;
            TurnManager.currentTurn = TurnManager.Turn.disabled;
        }

        if(waveNumber > numberOfWaves && timeLeft <= 0)
            GameManager.TheEnd();
    }

    bool anyEnemyIsReady()
    {
        for (int i = 0; i < spawnedEnemies.Count; i++)
        {
            if (spawnedEnemies[i].isOnStage)
            {                
                return true;
            }
        }
        
        return false;
    }

    int EnemiesAlive()
    {
        int count = 0;

        for (int i = 0; i < spawnedEnemies.Count; i++)
        {
            if (spawnedEnemies[i] != null)
                count++;
        }

        return count;
    }

    public int TimeLeft()
    {
        return (int)timeLeft;
    }

    IEnumerator NextWave()
    {
        timeLeft = timePerWave;
        delayLeft = delayBetweenWaves;

        if (waveNumber > numberOfWaves)
        {            
            yield break;
        }

        for (int i = 0; i < spawnedEnemies.Count; i++)
        {
            if(spawnedEnemies[i] != null)
                spawnedEnemies[i].Disappear();
        }

        spawnedEnemies.Clear();
        usedSpawnPoints.Clear();

        for (int i = 0; i < waves[waveNumber].enemies.Length; i++)
        {
            int randomIndex = 0;
            while (usedSpawnPoints.Contains(randomIndex))
                randomIndex = Random.Range(0, spawnPoints.Count);
            usedSpawnPoints.Add(randomIndex);

            Transform spawnPoint = spawnPoints[randomIndex];
            GameObject _enemy = Instantiate(waves[waveNumber].enemies[i].gameObject, spawnPoint.position, Quaternion.identity);
            spawnedEnemies.Add(_enemy.GetComponent<EnemyStats>());
            EnemyShooter enemyShooter = _enemy.GetComponent<EnemyShooter>();
            enemyShooter.entranceTarget = spawnPoint.GetChild(0);
            enemyShooter.exitTarget = spawnPoint;
            enemyShooter.timeOnStage = timePerWave - 1f;

            yield return new WaitForSeconds(0.1f);
        }

        waveNumber++;
    }
}
