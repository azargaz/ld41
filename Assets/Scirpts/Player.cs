﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player p;

    public float moveSpeed = 6;
    public float slowerMoveSpeed = 3;
    public float accelerationTime = 0.2f;
    Vector3 velocity;
    Vector3 velocitySmoothing;

    public int ammoPerTurn = 5;
    [HideInInspector]
    public int ammoLeft;
    public float shootDelay = 0.1f;
    float shootDelayTimeLeft = 0f;

    Vector3 lastPlayerPos;

    BulletShooter shooter;

    public enum PlayerMoveRadius { longest = 250, medium = 150, shortest = 80 };
    public PlayerMoveRadius curMoveRadius = PlayerMoveRadius.longest;
    float curMoveRadiusFloat = 0f;

    TurnManager.Turn lastTurn;

    public GameObject moveRadiusCircle;

    Vector3 spawnPoint;

    public GameObject left;
    public GameObject right;

    public GameObject hitboxMarker;

    public Bullet bomb;

    Animator anim;
    PlayerStats stats;

    AudioController audioController;

    /*
    bool playerTurn;
    public bool PlayerTurn
    {
        get { return playerTurn; }
        set
        {
            if (playerTurn == value) return;
            playerTurn = value;
            if (TurnChanged != null)
                TurnChanged();
        }
    }
    public delegate void OnTurnChange();
    public event OnTurnChange TurnChanged;
    */

    void Start()
    {
        #region Initialize stuff

        p = this;

        spawnPoint = transform.position;

        stats = GetComponent<PlayerStats>();
        anim = GetComponent<Animator>();
        audioController = GetComponentInChildren<AudioController>();

        //TurnChanged += TurnChange;

        lastPlayerPos = transform.position;
        curMoveRadius = GetPlayerMoveRadius();
        curMoveRadiusFloat = (float)curMoveRadius / 100f;
        shooter = GetComponent<BulletShooter>();

        StartCoroutine(Scale());

        #endregion
    }

    void TurnChange()
    {
        ammoLeft = ammoPerTurn;

        lastPlayerPos = transform.position;
        curMoveRadius = GetPlayerMoveRadius();
        curMoveRadiusFloat = (float)curMoveRadius / 100f;

        StopCoroutine("Scale");
        StartCoroutine(Scale());

        TurnManager.tm.curTurnLenght = curMoveRadius == PlayerMoveRadius.longest ? TurnManager.TurnLenght.longest : curMoveRadius == PlayerMoveRadius.medium ? TurnManager.TurnLenght.medium : TurnManager.TurnLenght.shortest;
    }

    void Update ()
    {
        if (stats.lives <= 0)
        {
            anim.SetTrigger("Death");
            return;
        }

        //PlayerTurn = TurnManager.currentTurn == TurnManager.Turn.player;

        if(Input.GetButtonDown("Bomb") && stats.bombs > 0)
        {
            audioController.Play(1);
            stats.bombs--;
            Bullet _bomb = Instantiate(bomb.gameObject, transform.position, Quaternion.identity).GetComponent<Bullet>();
            _bomb.timeToLive = 0.5f;

            if(TurnManager.currentTurn == TurnManager.Turn.enemy)
                TurnManager.tm.EndTurn(TurnManager.Turn.enemy);
        }

        anim.SetBool("FlyingUp", false);

        if (TurnManager.currentTurn == TurnManager.Turn.enemy)
            return;

        #region Move

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        anim.SetBool("FlyingUp", input.y > 0);

        bool slowdown = Input.GetButton("Slowdown");
        hitboxMarker.SetActive(slowdown);
        Vector3 targetVelocity = input * (slowdown ? slowerMoveSpeed : moveSpeed);

        velocity = Vector3.SmoothDamp(velocity, targetVelocity, ref velocitySmoothing, accelerationTime);

        float verticalCameraBoundary = Camera.main.orthographicSize;

        if (transform.position.y > verticalCameraBoundary && input.y > 0 || transform.position.y < -verticalCameraBoundary && input.y < 0)
            velocity.y = 0;

        if (input != Vector2.zero)
        {
            if(input.x > 0)
            {
                Vector3 rightPos = Camera.main.ScreenToWorldPoint(right.transform.position - right.GetComponent<BoxCollider2D>().bounds.extents);
                if (Mathf.Abs(rightPos.x - transform.position.x) < 0.2f)
                    velocity.x = 0;
            }
            else if(input.x < 0)
            {
                Vector3 leftPos = Camera.main.ScreenToWorldPoint(left.transform.position + left.GetComponent<BoxCollider2D>().bounds.extents);
                if (Mathf.Abs(leftPos.x - transform.position.x) < 0.2f)
                    velocity.x = 0;
            }
        }

        if(TurnManager.currentTurn != TurnManager.Turn.disabled)
            transform.Translate(velocity * Time.deltaTime);

        float distance = Vector3.Distance(transform.position, lastPlayerPos);
        if (distance > curMoveRadiusFloat)
        {
            Vector3 fromOriginToPlayer = transform.position - lastPlayerPos;
            fromOriginToPlayer *= curMoveRadiusFloat / distance;
            transform.position = lastPlayerPos + fromOriginToPlayer;
        }        
                
        #endregion

        #region Shoot

        if (Input.GetButton("Fire") && shootDelayTimeLeft <= 0 && ammoLeft > 0)
        {
            audioController.Play(0);
            ammoLeft--;
            shooter.Shoot();
            shootDelayTimeLeft = shootDelay;
        }
        shootDelayTimeLeft -= Time.deltaTime;

        #endregion

        #region End Turn

        TurnManager.Turn currentTurn = TurnManager.currentTurn;

        if(lastTurn != currentTurn)
            TurnChange();

        lastTurn = currentTurn;

        if (Input.GetButton("EndTurn") && TurnManager.currentTurn == TurnManager.Turn.player)
        {
            TurnChange();
            TurnManager.tm.EndTurn(TurnManager.Turn.player);            
        }

        #endregion

        if (TurnManager.currentTurn == TurnManager.Turn.disabled)
        {
            ammoLeft = 0;
            Vector3 direction = spawnPoint - transform.position;

            if (direction.magnitude > 0.1f)
            {
                transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);
                TurnChange();
            }
        }
    }

    Vector3 scaleSmoothing;

    IEnumerator Scale()
    {
        moveRadiusCircle.transform.Translate((lastPlayerPos - moveRadiusCircle.transform.position) * 5f * Time.deltaTime);
        Vector3 scale = new Vector3(curMoveRadiusFloat / 4f, curMoveRadiusFloat / 4f, 1f);
        Vector3 currentScale = scale;
        moveRadiusCircle.transform.localScale = Vector3.SmoothDamp(moveRadiusCircle.transform.localScale, currentScale, ref scaleSmoothing, 0.4f);

        if (moveRadiusCircle.transform.localScale == currentScale)
            yield return null;

        yield return new WaitForEndOfFrame();
        StartCoroutine(Scale());
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(lastPlayerPos, curMoveRadiusFloat);
    }

    #region GetPlayerMoveRadius

    public CircleCollider2D mediumCollider;
    public CircleCollider2D smallCollider;

    PlayerMoveRadius GetPlayerMoveRadius()
    {
        LayerMask mask = 1 << 10;

        if (smallCollider.IsTouchingLayers(mask))
            return PlayerMoveRadius.shortest;
        else if (mediumCollider.IsTouchingLayers(mask))
            return PlayerMoveRadius.medium;

        return PlayerMoveRadius.longest;
    }

    #endregion
}
