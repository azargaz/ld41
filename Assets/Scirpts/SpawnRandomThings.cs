﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomThings : MonoBehaviour {

    public Bullet[] thingsToSpawn;
    public int delay;
    float timeLeft;


	void Update ()
    {
        timeLeft -= Time.deltaTime;

        if (timeLeft <= 0)
            Spawn();
	}

    void Spawn()
    {
        timeLeft = delay;
        Bullet _thing = Instantiate(thingsToSpawn[Random.Range(0, thingsToSpawn.Length)].gameObject, transform.position, Quaternion.identity).GetComponent<Bullet>();
        _thing.direction = Vector2.down;
        _thing.timeToLive = delay;
    }
}
