﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour {

    public Bullet bullet;

    public float initialDelayBetweenAttacks = 1f;
    public float delayBetweenAttacks = 3f;
    float delayBetweenAttacksTimeLeft = 0f;

    public AttackPattern[] attacks;

    public Transform[] firePoints;

    [System.Serializable]
    public class AttackPattern
    {
        public int bullletsPerWave = 10;
        public int waves = 1;
        public float bulletsInterval;
        public float wavesInterval;
        public float startingRotation;
        public float bulletCone;
        public float waveRotationStep;
        public int changeDirectionEveryXwave = 1;
    }

    public float moveSpeed;

    public Transform entranceTarget;
    int currentWaypoint = 0;
    [HideInInspector]
    public bool isOnStage = false;
    public float timeOnStage = 0f;
    float timeOnStageLeft = 0f;
    public Transform exitTarget;
    AudioController audioController;

    void Start()
    {
        audioController = GetComponentInChildren<AudioController>();

        timeOnStageLeft = timeOnStage;
        delayBetweenAttacksTimeLeft = initialDelayBetweenAttacks;
        Move();
    }

    void Update()
    {
        if(TurnManager.currentTurn == TurnManager.Turn.enemy)
            timeOnStageLeft -= Time.deltaTime;

        if (!CheckIfEnemiesTurn())
            return;

        if (!isOnStage || timeOnStageLeft <= 0f)
        {
            Move();
            return;
        }

        if (TurnManager.currentTurn == TurnManager.Turn.disabled)
            return;              

        delayBetweenAttacksTimeLeft -= Time.deltaTime;

        if (delayBetweenAttacksTimeLeft <= 0)
        {            
            StartCoroutine(Attack(attacks[Random.Range(0, attacks.Length)]));
        }
    }

    void Move()
    {
        Vector3 target = isOnStage ? exitTarget.position : entranceTarget.position;

        Vector3 direction = target - transform.position;

        if (direction.magnitude > 0.1f)
            transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);
        else
        {
            isOnStage = direction.magnitude <= 0.1f;
            return;
        }
    }

    IEnumerator Attack(AttackPattern pattern)
    {
        float startRotation = pattern.startingRotation;

        for (int i = 0; i < pattern.waves; i++)
        {
            for (int j = 0; j < pattern.bullletsPerWave; j++)
            {
                if(TurnManager.currentTurn != TurnManager.Turn.enemy)
                    yield return new WaitUntil(CheckIfEnemiesTurn);

                for (int k = 0; k < firePoints.Length; k++)
                {
                    delayBetweenAttacksTimeLeft = delayBetweenAttacks;

                    Bullet _bullet = Instantiate(bullet.gameObject, firePoints[k].position, Quaternion.identity).GetComponent<Bullet>();
                    float angleInRads;
                    if (pattern.bullletsPerWave > 1)
                    {
                        angleInRads = ((180 - pattern.bulletCone) / 2 + j * pattern.bulletCone / (pattern.bullletsPerWave) + startRotation) * Mathf.PI / 180;
                    }
                    else
                        angleInRads = ((180 - pattern.bulletCone) / 2 + startRotation) * Mathf.PI / 180;

                    _bullet.direction = new Vector2(Mathf.Cos(angleInRads), Mathf.Sin(angleInRads));                    
                }
                
                if (pattern.bulletsInterval > 0.2f)
                {
                    audioController.Play(0);
                    yield return new WaitForSeconds(pattern.bulletsInterval);
                }
            }

            if (pattern.bulletsInterval <= 0.2f)
                audioController.Play(0);

            startRotation += pattern.waveRotationStep * (pattern.changeDirectionEveryXwave == 0 ? 1 : i % pattern.changeDirectionEveryXwave == 0 ? 1 : -1);

            if(pattern.wavesInterval > 0)
                yield return new WaitForSeconds(pattern.wavesInterval);
        }
    }

    bool CheckIfEnemiesTurn()
    {
        return TurnManager.currentTurn == TurnManager.Turn.enemy || TurnManager.currentTurn == TurnManager.Turn.disabled;
    }
}