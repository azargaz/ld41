﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour {

    public Bullet bullet;

    public float bulletCone = 45;
    public float bulletConeRotation = 180;
    public int bulletsPerAttack = 10;

    float bulletSalvaRotation = 0;
    public float bulletSalvaRotationStep = 10;
    public float bulletSalvaTimeStep = 0.01f;
    public int salva = 100;
    int salvaLeft = 0;

    public Transform firePoint;

	void Start ()
    {
        salvaLeft = salva;
	}
	
	void Update ()
    {
		
	}

    public IEnumerator Salva()
    {        
        Bullet _bullet = Instantiate(bullet.gameObject, transform.position, Quaternion.identity).GetComponent<Bullet>();
        float angleInRads = (bulletSalvaRotation) * Mathf.PI / 180;
        salvaLeft--;
        bulletSalvaRotation += bulletSalvaRotationStep * (Random.value > 0.5f ? 1 : -1);
        _bullet.direction = new Vector2(Mathf.Cos(angleInRads), Mathf.Sin(angleInRads));
        if (salvaLeft > 0)
        {
            yield return new WaitForSeconds(bulletSalvaTimeStep);
            StartCoroutine(Salva());
        }
        else
        {
            bulletSalvaRotation = 270;
            salvaLeft = salva;
            yield return null;
        }
    }

    public void Shoot()
    {
        for (int i = 0; i < bulletsPerAttack; i++)
        {
            Bullet _bullet = Instantiate(bullet.gameObject, firePoint.position, Quaternion.identity).GetComponent<Bullet>();
            float angleInRads;
            if (bulletsPerAttack > 1)
            {
                angleInRads = ((180 - bulletCone) / 2 + i * bulletCone / (bulletsPerAttack - 1) + bulletConeRotation) * Mathf.PI / 180;
            }
            else
                angleInRads = ((180 - bulletCone) / 2 + bulletConeRotation) * Mathf.PI / 180;

            _bullet.direction = new Vector2(Mathf.Cos(angleInRads), Mathf.Sin(angleInRads));
        }
    }
}
