﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

    public static UI ui;

    public GameObject turn;
    Animator turnAnim;

    public GameObject help;

    public GameObject waves;
    public GameObject timePerWave;

    public GameObject ammo;

    public GameObject highscore;
    public GameObject score;

    public GameObject lives;
    public GameObject liveImg;
    List<GameObject> instantiatedLives = new List<GameObject>();
    public GameObject bombs;
    public GameObject bombImg;
    List<GameObject> instantiatedBombs = new List<GameObject>();
    
    float lastScoreFloat = 0;
    float lastHighscoreFloat = 0;
    float lastAmmoFloat = 0;

    Animator uiAnim;

    void Start()
    {
        ui = this;
        uiAnim = GetComponent<Animator>();
        turnAnim = turn.GetComponent<Animator>();
    }

	void Update ()
    {
        float scoreFloat = UpdateTextCounter(lastScoreFloat, PlayerStats.ps.score);
        float highscoreFloat = UpdateTextCounter(lastHighscoreFloat, SaveLoad.highscore);
        float ammoFloat = UpdateTextCounter(lastAmmoFloat, Player.p.ammoLeft);

        score.GetComponent<Text>().text = "Score: " + Mathf.RoundToInt(scoreFloat);
        highscore.GetComponent<Text>().text = "Highscore: " + Mathf.RoundToInt(highscoreFloat);
        ammo.GetComponent<Text>().text = "Ammo: " + Mathf.RoundToInt(ammoFloat);

        string[] wavesInRoman = { "I", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII" };
        waves.GetComponent<Text>().text = "Wave " + wavesInRoman[WaveManager.waveNumber];
        timePerWave.GetComponent<Text>().text = "Time left: " + WaveManager.wm.TimeLeft();

        lastScoreFloat = scoreFloat;
        lastHighscoreFloat = highscoreFloat;
        lastAmmoFloat = ammoFloat;
        
        UpdateImgCounter(instantiatedLives, liveImg, lives, PlayerStats.ps.lives);
        UpdateImgCounter(instantiatedBombs, bombImg, bombs, PlayerStats.ps.bombs);

        turnAnim.SetBool("Disabled", TurnManager.currentTurn == TurnManager.Turn.disabled);
        turnAnim.SetBool("Player", TurnManager.currentTurn == TurnManager.Turn.player);
        turnAnim.SetBool("Enemy", TurnManager.currentTurn == TurnManager.Turn.enemy);

        if (Input.GetKeyDown(KeyCode.H))
            help.SetActive(!help.activeInHierarchy);
    }

    float UpdateTextCounter(float value, float target)
    {
        return Mathf.Lerp(value, target, (Mathf.Abs(target - value) > 10) ? 0.2f : 0.5f);
    }    

    void UpdateImgCounter(List<GameObject> instantiatedImgs, GameObject img, GameObject parent, int count)
    {
        if (instantiatedImgs.Count > count)
        {
            for (int i = 0; i < instantiatedImgs.Count - count; i++)
            {
                Destroy(instantiatedImgs[i].gameObject);
                instantiatedImgs.RemoveAt(i);
            }
        }
        else if (instantiatedImgs.Count < count)
        {
            for (int i = 0; i < count - instantiatedImgs.Count; i++)
            {
                GameObject _img = Instantiate(img);
                _img.transform.SetParent(parent.transform);
                _img.transform.position = parent.transform.position;
                instantiatedImgs.Add(_img);
            }
        }
        else
            return;
    }

    public void GameOver()
    {
        uiAnim.SetTrigger("GameOver");
    }

    public void TheEnd()
    {
        uiAnim.SetTrigger("TheEnd");
    }

    public void ButtonMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ButtonRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
