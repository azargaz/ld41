﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour {

    public static TurnManager tm;
    public enum Turn { player, enemy, disabled };
    public static Turn currentTurn = Turn.disabled;
    public enum TurnLenght { longest = 100, medium = 50, shortest = 25};
    public TurnLenght curTurnLenght = TurnLenght.longest;

    float enemyTurnTimeLeft = 0f;

    void Awake ()
    {
        tm = this;
        currentTurn = Turn.disabled;
        enemyTurnTimeLeft = (float)curTurnLenght / 100f;
	}    

    void Update()
    {
        if(currentTurn == Turn.enemy)
        {
            enemyTurnTimeLeft -= Time.deltaTime;
            if (enemyTurnTimeLeft <= 0)
                EndTurn(Turn.enemy);
        }

        if(currentTurn == Turn.disabled)
        {
            Bullet[] bullets = FindObjectsOfType<Bullet>();
            for (int i = 0; i < bullets.Length; i++)
            {
                Destroy(bullets[i].gameObject);
            }
        }
    }
	
	public void EndTurn(Turn endingTurn)
    {
        currentTurn = (endingTurn == Turn.player) ? Turn.enemy : Turn.player;
        if (currentTurn == Turn.disabled)
            currentTurn = Turn.player;

        if (currentTurn == Turn.enemy)
            enemyTurnTimeLeft = (float)curTurnLenght / 100f;
    }
}
