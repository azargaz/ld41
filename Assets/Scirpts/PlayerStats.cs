﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public static PlayerStats ps;

    public int score;
    int lifeScore;
    int bombScore;
    public int bonusLifeThreshold;
    public int bonusBombsThreshold;

    public int lives = 3;
    public int maxLives = 4;
    public int bombs = 1;
    public int maxBombs = 3;

    bool gameover = false;

    public bool invincible = false;
    public float invincibilityDuration = 2f;
    float invincibilityDurationTimeLeft = 0f;

    SpriteRenderer sprite;
    AudioController audioController;

    void Start()
    {
        ps = this;
        sprite = GetComponent<SpriteRenderer>();
        audioController = GetComponentInChildren<AudioController>();

        StartCoroutine(InvincibilityEffect());
	}

    void Update()
    {
        invincible = invincibilityDurationTimeLeft > 0;
        invincibilityDurationTimeLeft -= Time.deltaTime;

        if (lives <= 0 && !gameover)
            GameOver();
    }

    bool IsInvincible()
    {
        return invincible;
    }

    IEnumerator InvincibilityEffect()
    {
        yield return new WaitUntil(IsInvincible);

        sprite.color = new Color(0.5f, 0.5f, 0.5f);
        yield return new WaitForSeconds(0.25f);
        sprite.color = new Color(1f, 1f, 1f);
        yield return new WaitForSeconds(0.25f);

        StartCoroutine(InvincibilityEffect());
    }

    public void AddScore(int amount)
    {
        score += amount;
        lifeScore += amount;
        bombScore += amount;

        if (lifeScore >= bonusLifeThreshold)
            AddLife();
        if (bombScore >= bonusBombsThreshold)
            AddBomb();
    }

    public void AddLife()
    {
        lifeScore -= bonusLifeThreshold;

        if (lives < maxLives)
            lives++;
    }

    public void AddBomb()
    {
        bombScore -= bonusBombsThreshold;

        if (bombs < maxBombs)
            bombs++;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer == 10)
        {
            Damage();            
        }
    }

    void Damage()
    {
        if (invincible)
            return;

        invincibilityDurationTimeLeft = invincibilityDuration;
        invincible = true;

        if (lives >= 1)
            lives--;
    }

    void GameOver()
    {
        gameover = true;
        audioController.Play(2);
        GameManager.GameOver();
    }
}
