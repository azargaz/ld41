﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float moveSpeed = 3f;
    [HideInInspector]
    public Vector2 direction;
    public float timeToLive = 5f;
    public bool enemyBullet = true;
    public bool destroyBullets = false;
    public bool rotate = false;

	void Update ()
    {
        if(TurnManager.currentTurn != TurnManager.Turn.disabled)
        {
            if (enemyBullet && TurnManager.currentTurn != TurnManager.Turn.enemy)
                return;

            if (!enemyBullet && TurnManager.currentTurn != TurnManager.Turn.player)
                return;
        }

        transform.Translate(direction * Time.deltaTime * moveSpeed);
        if(rotate)
            transform.GetChild(0).eulerAngles = new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI);

        timeToLive -= Time.deltaTime;
        if (timeToLive <= 0)
            BeforeDestroy();
	}

    protected virtual void BeforeDestroy()
    {
        Destroy(gameObject);
    }

    public void DestroyNow()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 12)
            DestroyNow();

        if (enemyBullet || destroyBullets)
            return;

        if (other.gameObject.layer == 9)
            BeforeDestroy();
    }
}
