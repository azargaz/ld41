﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoad : MonoBehaviour {

    public static SaveLoad SL;
    public static int highscore;

	void Start ()
    {
        SL = this;
        Load();
	}

    public void AddHighscore(int score)
    {
        if (score > highscore)
        {
            highscore = score;
            Save();
        }
        else
            return;
    }

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/highscore.hj");

        bf.Serialize(file, highscore);
        file.Close();

        Debug.Log("Highscore saved.");
    }

    void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/highscore.hj"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/highscore.hj", FileMode.Open);
            highscore = (int)bf.Deserialize(file);

            file.Close();
            Debug.Log("Highscore loaded.");
        }
    }
}
