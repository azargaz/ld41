﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentingBullet : Bullet {

    public int fragments = 1;
    public Bullet fragmentBullet;

    protected override void BeforeDestroy()
    {
        for (int i = 0; i < fragments; i++)
        {
            GameObject _fragment = Instantiate(fragmentBullet.gameObject, transform.position, Quaternion.identity);
            Bullet _bullet = _fragment.GetComponent<Bullet>();
            float angle = 2 * Mathf.PI / fragments * i;
            Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
            _bullet.direction = direction;
        }

        base.BeforeDestroy();
    }
}
