﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    /*
    For player:
    clips[0] = shooting sfx
    clips[1] = bomb sfx
    clips[2] = death sfx

    For enemies:
    clisp[0] = shooting sfx
    */
    public AudioClip[] clips;
    AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

	public void Play(int index)
    {
        source.clip = clips[index];
        source.Play();
    }
}
