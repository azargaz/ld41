﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static void GameOver()
    {
        SaveLoad.SL.AddHighscore(PlayerStats.ps.score);
        UI.ui.GameOver();
    }

    public static void TheEnd()
    {
        SaveLoad.SL.AddHighscore(PlayerStats.ps.score);
        UI.ui.TheEnd();
    }
}
